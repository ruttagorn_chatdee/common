package th.co.digio.common.dto.sftp;

import lombok.Data;
import th.co.digio.common.enumuration.ResponseStatus;

@Data
public class SFTPFileDTO {
    private ResponseStatus status = ResponseStatus.ERROR_EXCEPTION_OCCURRED;
    private boolean fileFound = false;
    private String description;
}
