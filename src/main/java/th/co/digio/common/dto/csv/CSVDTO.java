package th.co.digio.common.dto.csv;

import lombok.Data;

@Data
public class CSVDTO {
    private String text;
}
