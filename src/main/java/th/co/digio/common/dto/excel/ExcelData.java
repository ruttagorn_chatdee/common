package th.co.digio.common.dto.excel;

import lombok.Data;

import java.util.List;

@Data
public class ExcelData {
    List<ExcelDTO> data;
}
