package th.co.digio.common.dto.excel;

import lombok.Data;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

@Data
public class ExcelDTO {
    private String text;
    private String dataFormat;

    private HorizontalAlignment alignment = HorizontalAlignment.CENTER;

    private boolean border = true;
    private boolean bold = false;
    private boolean percent = false;

    private CellType cellType = CellType.STRING;
}
