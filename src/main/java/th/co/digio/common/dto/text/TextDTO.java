package th.co.digio.common.dto.text;

import lombok.Data;

@Data
public class TextDTO {
    String delimiter = "|";
    boolean padding;
    boolean back;
}
