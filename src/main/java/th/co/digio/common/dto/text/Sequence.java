package th.co.digio.common.dto.text;

import lombok.Data;

@Data
public class Sequence {
    private Integer sequence;
}
