package th.co.digio.common.dto;

import lombok.Data;

@Data
public class GetRequestDTO {
    boolean success = true;
    String message;
    String response;
}
