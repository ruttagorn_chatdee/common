package th.co.digio.common.dto.pageable;

import lombok.Data;
import org.springframework.data.domain.Pageable;
import th.co.digio.common.enumuration.ResponseStatus;

@Data
public class PageableDTO {
    private Pageable pageable;
    private ResponseStatus status;
}
