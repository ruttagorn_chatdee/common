package th.co.digio.common.dto.csv;

import lombok.Data;

@Data
public class CSVSuffixDTO {
    private boolean hasDate = false;
    private boolean hasRandomSuffix = false;
    private boolean isUpperExtensions = false;
    private String charset = "UTF-8";
}
