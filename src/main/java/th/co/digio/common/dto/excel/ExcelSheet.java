package th.co.digio.common.dto.excel;

import lombok.Data;

import java.util.List;

@Data
public class ExcelSheet {
    List<ExcelData> row;
    String sheetName;

    public ExcelSheet() { }

    public ExcelSheet(String sheetName) {
        this.sheetName = sheetName;
    }

    public ExcelSheet(String sheetName, List<ExcelData> row) {
        this.sheetName = sheetName;
        this.row = row;
    }
}
