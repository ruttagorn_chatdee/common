package th.co.digio.common.dto.excel;

import lombok.Data;
import org.apache.poi.ss.usermodel.CellStyle;

@Data
public class ExcelStyleDTO {
    private CellStyle borderBoldLeft;
    private CellStyle borderBoldCenter;
    private CellStyle borderBoldRight;
    private CellStyle borderLeft;
    private CellStyle borderCenter;
    private CellStyle borderRight;
    private CellStyle boldLeft;
    private CellStyle boldCenter;
    private CellStyle boldRight;
    private CellStyle left;
    private CellStyle center;
    private CellStyle right;
}
