package th.co.digio.common.dto.csv;

import lombok.Data;

import java.util.List;

@Data
public class CSVData {
    List<String> data;
    boolean hasDoubleQuote = true;
}
