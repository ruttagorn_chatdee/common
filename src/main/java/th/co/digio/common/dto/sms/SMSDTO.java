package th.co.digio.common.dto.sms;

import lombok.Data;

@Data
public class SMSDTO {
    String username;
    String password;
    String senderName;
    String msisdn;
    String message;
}
