package th.co.digio.common.dto.pageable;

import lombok.Data;
import org.springframework.data.domain.Page;

@Data
public class PageDTO {
    private Object content;
    private long pageNumber = 0;
    private long pageSize = 0;
    private long totalPages = 0;
    private long totalElements = 0;
    private long numberOfElements = 0;
    private boolean first = true;
    private boolean last = false;

    public PageDTO() {
    }

    public PageDTO(Page<?> page) {
        this.pageNumber = page.getNumber();
        this.pageSize = page.getSize();
        this.totalPages = page.getTotalPages();
        this.totalElements = page.getTotalElements();
        this.numberOfElements = page.getNumberOfElements();
        this.first = page.isFirst();
        this.last = page.isLast();
    }
}
