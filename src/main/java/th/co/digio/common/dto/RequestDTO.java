package th.co.digio.common.dto;

import lombok.Data;

@Data
public class RequestDTO {
    private String pageNumber;
    private String pageSize;
}
