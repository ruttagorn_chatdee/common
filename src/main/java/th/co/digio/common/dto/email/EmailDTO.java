package th.co.digio.common.dto.email;

import lombok.Data;
import th.co.digio.common.enumuration.ResponseStatus;

@Data
public class EmailDTO {
    private ResponseStatus status = ResponseStatus.ERROR_EXCEPTION_OCCURRED;
    private String error;
}
