package th.co.digio.common.dto;

import lombok.Data;

@Data
public class ResponseBodyDTO {
    private String code;
    private String message;
    private Object data;
}
