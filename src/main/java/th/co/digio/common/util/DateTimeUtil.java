package th.co.digio.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.chrono.ThaiBuddhistChronology;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTimeUtil extends CommonUtil {

    private static final Logger logger = LogManager.getLogger(DateTimeUtil.class);

    public static boolean checkDateFormat(String date, String format) {
        return checkDateFormat(date, format, null);
    }

    public static boolean checkDateFormat(String date, String format, Locale locale) {
        try {
            SimpleDateFormat simpleDateFormat;
            if (locale == null) {
                simpleDateFormat = new SimpleDateFormat(format);
            } else {
                simpleDateFormat = new SimpleDateFormat(format, locale);
            }
            simpleDateFormat.setLenient(false);
            simpleDateFormat.parse(date);
        } catch (Exception e) {
            logger.info("[" + date + "] date format is incorrect. Date format is " + format);
            return false;
        }
        return true;
    }

    public static String setStringFormatFromDate(LocalDateTime date, String format) {
        return setStringFormatFromDate(date, format, null);
    }

    public static String setStringFormatFromDate(LocalDateTime date, String format, Locale locale) {
        String datetime = "";
        try {
            if (locale == null) {
                datetime = date.format(DateTimeFormatter.ofPattern(format));
            } else if (locale == localeTh) {
                datetime = date.format(DateTimeFormatter.ofPattern(format).withChronology(ThaiBuddhistChronology.INSTANCE));
            } else {
                datetime = date.format(DateTimeFormatter.ofPattern(format, locale));
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return datetime;
    }

    public static String setStringFormatFromDate(Date date, String format) {
        return setStringFormatFromDate(date, format, null);
    }

    public static String setStringFormatFromDate(Date date, String format, Locale locale) {
        String datetime = "";
        try {
            if (locale == null) {
                datetime = new SimpleDateFormat(format).format(date);
            } else {
                datetime = new SimpleDateFormat(format, locale).format(date);
            }
        } catch (Exception e) {
            logger.error(e);
        }
        return datetime;
    }

    public static Date setDateFormatFromString(String date, String format) {
        return setDateFormatFromString(date, format, null);
    }

    public static Date setDateFormatFromString(String date, String format, Locale locale) {
        Date datetime = null;
        try {
            if (locale == null) {
                datetime = new SimpleDateFormat(format).parse(date);
            } else {
                datetime = new SimpleDateFormat(format, locale).parse(date);
            }
        } catch (Exception e) {
            logger.info("[" + date + "] date format is incorrect. Date format is " + format);
        }
        return datetime;
    }

    public static String changeDateFormat(String date, String from, String to) {
        if (!checkDateFormat(date, from, null)) {
            return null;
        }

        return setStringFormatFromDate(setDateFormatFromString(date, from, null), to, null);
    }

    public static String changeDateFormat(String date, String from, String to, Locale localeFrom, Locale localeTo) {
        if (!checkDateFormat(date, from, localeFrom)) {
            return null;
        }

        return setStringFormatFromDate(setDateFormatFromString(date, from, localeFrom), to, localeTo);
    }

    public static Date startDateFormat(String text, String format) {
        return startDateFormat(text, format, null);
    }

    public static Date startDateFormat(String text, String format, Locale locale) {
        return setDateFormatFromString(text, format, locale);
    }

    public static Date stopDateFormat(String text, String format) {
        return stopDateFormat(text, format, null);
    }

    public static Date stopDateFormat(String text, String format, Locale locale) {
        Date date = setDateFormatFromString(text, format, locale);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar.getTime();
    }

    public static String setYearBCToAD(String date, String format) {
        return changeDateFormat(date, format, format, localeTh, null);
    }

    public static String setYearADToBC(String date, String format) {
        return changeDateFormat(date, format, format, null, localeTh);
    }

    public static long getDifferenceDateTime(ChronoUnit type, Date first, Date second) {
        return type.between(first.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(), second.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
    }

    public static long getDifferenceDateTime(ChronoUnit type, LocalDateTime first, LocalDateTime second) {
        return type.between(first, second);
    }

    public static boolean isOverDateTime(ChronoUnit type, Date first, Date second, long maximum) {
        return getDifferenceDateTime(type, first, second) > maximum;
    }

    public static boolean isOverDateTime(ChronoUnit type, LocalDateTime first, LocalDateTime second, long maximum) {
        return getDifferenceDateTime(type, first, second) > maximum;
    }
}
