package th.co.digio.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.sun.mail.smtp.SMTPTransport;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import th.co.digio.common.dto.GetRequestDTO;
import th.co.digio.common.dto.PostRequestDTO;
import th.co.digio.common.dto.RequestDTO;
import th.co.digio.common.dto.ResponseBodyDTO;
import th.co.digio.common.dto.email.EmailDTO;
import th.co.digio.common.dto.pageable.PageableDTO;
import th.co.digio.common.dto.sms.SMSDTO;
import th.co.digio.common.dto.text.Sequence;
import th.co.digio.common.dto.text.TextDTO;
import th.co.digio.common.enumuration.FileType;
import th.co.digio.common.enumuration.ResponseStatus;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static th.co.digio.common.util.DateTimeUtil.setStringFormatFromDate;

public class CommonUtil {

    private static final Logger logger = LogManager.getLogger(CommonUtil.class);

    public static String languageThai = "TH";

    public static Locale localeTh = new Locale("th", "th");

    public static String fileNameDateTimeFormat = "yyyyMMddHHmmss";

    public static String contentType = "application/json";

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.length() == 0;
    }

    public static boolean isNumeric(String text) {
        if (isNullOrEmpty(text)) {
            return false;
        }

        for (int i = 0; i < text.length(); i++) {
            if (!Character.isDigit(text.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static String generateUUID() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    public static String generateUniqueId() {
        return generateUUID() + "-" + generateUUID();
    }

    public static String parseObjectToJSON(Object object) {
        return parseObjectToJSON(object, PropertyNamingStrategy.SNAKE_CASE);
    }

    public static String parseObjectToJSON(Object object, PropertyNamingStrategy propertyNamingStrategies) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(propertyNamingStrategies);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();

        byte[] jsonBytes;
        try {
            jsonBytes = ow.writeValueAsBytes(object);
            return new String(jsonBytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error(e);
        }

        return null;
    }

    public static BigDecimal parseBigIntegerToBigDecimal(BigInteger bigIntegerNumber) {
        BigDecimal number = null;
        if (bigIntegerNumber == null) {
            return null;
        }

        try {
            number = new BigDecimal(bigIntegerNumber);
        } catch (Exception e) {
            logger.error(e);
        }
        return number;
    }

    public static BigDecimal parseDoubleToBigDecimal(Double doubleNumber) {
        BigDecimal number = null;
        if (doubleNumber == null) {
            return null;
        }

        try {
            number = new BigDecimal(doubleNumber);
        } catch (Exception e) {
            logger.error(e);
        }
        return number;
    }

    public static Double parseLongToDouble(Long longNumber) {
        Double number = null;
        if (longNumber == null) {
            return null;
        }

        try {
            number = longNumber.doubleValue();
        } catch (Exception e) {
            logger.error(e);
        }
        return number;
    }

    public static Long parseDoubleToLong(Double doubleNumber) {
        Long number = null;
        if (doubleNumber == null) {
            return null;
        }

        try {
            number = doubleNumber.longValue();
        } catch (Exception e) {
            logger.error(e);
        }
        return number;
    }

    public static Integer parseLongToInteger(Long longNumber) {
        Integer number = null;
        if (longNumber == null) {
            return null;
        }

        try {
            number = longNumber.intValue();
        } catch (Exception e) {
            logger.error(e);
        }
        return number;
    }

    public static Long parseIntegerToLong(Integer integerNumber) {
        Long number = null;
        if (integerNumber == null) {
            return null;
        }

        try {
            number = integerNumber.longValue();
        } catch (Exception e) {
            logger.error(e);
        }
        return number;
    }

    public static boolean download(File file) {
        if (file == null) {
            logger.error("file is null");
            return false;
        }

        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getResponse();
        if (response == null) {
            logger.error("response is null");
            return false;
        }
        try {
            if (!file.exists()) {
                logger.info("file :[" + file.getName() + "] is not found create empty file " + (file.createNewFile() ? "success" : "fail"));
            }

            String filename = file.getName();
            filename = filename.substring(0, filename.lastIndexOf("_")) + filename.substring(filename.lastIndexOf("."));

            int ContentLength = Integer.parseInt(String.valueOf(file.length()));

            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            response.setHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
            response.setContentLength(ContentLength);
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            FileCopyUtils.copy(inputStream, response.getOutputStream());
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    public static boolean validateRegex(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        return matcher.matches();
    }

    public static boolean validateRegexFind(String text, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(text);
        return matcher.find();
    }

    public static boolean validateTaxId(String taxId) {
        if (isNullOrEmpty(taxId)) {
            return false;
        }

        if (!isNumeric(taxId)) {
            return false;
        }

        if (taxId.length() != 13) {
            return false;
        }

        int sum = 0;
        for (int i = 0; i < 12; i++) {
            sum += Character.getNumericValue(taxId.charAt(i)) * (13 - i);
        }

        int correctSumTaxId = (11 - sum % 11) % 10;
        int sumTaxId = Character.getNumericValue(taxId.charAt(12));
        return correctSumTaxId == sumTaxId;
    }

    public static String setFinancialAmount(Long amount) {
        BigInteger bigInteger = BigInteger.valueOf(amount);
        return setFinancialAmount(bigInteger, 2, 0, false);
    }

    public static String setFinancialAmount(Long amount, boolean hasComma) {
        BigInteger bigInteger = BigInteger.valueOf(amount);
        return setFinancialAmount(bigInteger, 2, 0, hasComma);
    }

    public static String setFinancialAmount(Long amount, Integer decimal, Integer zeroLeading, boolean hasComma) {
        BigInteger bigInteger = BigInteger.valueOf(amount);
        return setFinancialAmount(bigInteger, decimal, zeroLeading, hasComma);
    }

    public static String setFinancialAmount(BigInteger amount) {
        return setFinancialAmount(amount, 0, 0, false);
    }

    public static String setFinancialAmount(BigInteger amount, boolean hasComma) {
        return setFinancialAmount(amount, 0, 0, hasComma);
    }

    public static String setFinancialAmount(BigInteger amount, Integer decimal, Integer zeroLeading, boolean hasComma) {
        if (amount == null) {
            amount = BigInteger.ZERO;
        }

        if (decimal == null) {
            decimal = 2;
        }

        if (zeroLeading == null) {
            zeroLeading = 0;
        }

        String format = "%";

        if (hasComma) {
            format += ",";
        }

        if (zeroLeading != 0) {
            format += "0" + zeroLeading;
        }

        format += "." + decimal + "f";

        BigDecimal divide = new BigDecimal(BigInteger.TEN.pow(decimal));
        BigDecimal num = parseBigIntegerToBigDecimal(amount).divide(divide);
        return String.format(format, num);
    }

    public static BigInteger setFinancialAmount(String amount) {
        return setFinancialAmount(amount, null);
    }

    public static BigInteger setFinancialAmount(String amount, Integer decimal) {
        if (isNullOrEmpty(amount)) {
            amount = "0";
        }

        if (decimal == null) {
            decimal = 2;
        }

        BigDecimal number;
        try {
            number = new BigDecimal(amount);
        } catch (Exception e) {
            logger.error(e);
            return null;
        }

        number = number.multiply(BigDecimal.TEN.pow(decimal));
        return number.toBigInteger();
    }

    public static String setTrim(String text) {
        if (isNullOrEmpty(text)) {
            text = "";
        }

        return text.trim();
    }

    public static EmailDTO sendEmail(List<String> listEmail, String subject, String body, String host, String port, String username, String password, String sendFrom, String fullName) {
        return sendEmail(listEmail, new ArrayList<>(), subject, body, host, port, username, password, sendFrom, fullName, false);
    }

    public static EmailDTO sendEmail(List<String> listEmail, File file, String subject, String body, String host, String port, String username, String password, String sendFrom, String fullName) {
        List<File> listFile = new ArrayList<>();
        listFile.add(file);
        return sendEmail(listEmail, listFile, subject, body, host, port, username, password, sendFrom, fullName, false);
    }

    public static EmailDTO sendEmail(List<String> listEmail, String subject, String body, String host, String port, String username, String password, String sendFrom, String fullName, boolean deleteSuffix) {
        return sendEmail(listEmail, new ArrayList<>(), subject, body, host, port, username, password, sendFrom, fullName, deleteSuffix);
    }

    public static EmailDTO sendEmail(List<String> listEmail, File file, String subject, String body, String host, String port, String username, String password, String sendFrom, String fullName, boolean deleteSuffix) {
        List<File> listFile = new ArrayList<>();
        listFile.add(file);
        return sendEmail(listEmail, listFile, subject, body, host, port, username, password, sendFrom, fullName, deleteSuffix);
    }

    public static EmailDTO sendEmail(List<String> listEmail, List<File> listFile, String subject, String body, String host, String port, String username, String password, String sendFrom, String fullName, boolean deleteSuffix) {
        return sendEmail(listEmail, new ArrayList<>(), listFile, subject, body, host, port, username, password, sendFrom, fullName, deleteSuffix);
    }

    public static EmailDTO sendEmail(List<String> listToEmail, List<String> listCcEmail, List<File> listFile, String subject, String body, String host, String port, String username, String password, String sendFrom, String fullName, boolean deleteSuffix) {
        EmailDTO emailDTO = new EmailDTO();
        String error = "";
        String charset = StandardCharsets.UTF_8.displayName();
        String encoding = "B";

        Properties props = new Properties();
        props.put("mail.smtp.socketFactory.port", port);
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");

        Session session = Session.getInstance(props, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        listToEmail = listToEmail.stream().distinct().collect(Collectors.toList());
        listCcEmail = listCcEmail.stream().distinct().collect(Collectors.toList());

        if (listToEmail.isEmpty()) {
            error = "email not found to send";
            logger.error(error);
            emailDTO.setError(error);
            return emailDTO;
        }

        try {
            Message message = new MimeMessage(session);
            InternetAddress address = new InternetAddress(sendFrom, fullName);
            message.setFrom(address);

            for (String email : listToEmail) {
                message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
            }

            for (String email : listCcEmail) {
                message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(email));
            }
            message.setSubject(MimeUtility.encodeText(subject, charset, encoding));

            Multipart multipart = new MimeMultipart();

            if (listFile == null) {
                listFile = new ArrayList<>();
            }

            MimeBodyPart messageBodyPart;

            for (File file : listFile) {
                messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(file);
                messageBodyPart.setDataHandler(new DataHandler(source));
                String fileName = file.getName();
                if (deleteSuffix) {
                    fileName = fileName.substring(0, fileName.lastIndexOf("_")) + fileName.substring(fileName.lastIndexOf("."));
                }

                logger.info("attach file:[" + fileName + "]");
                messageBodyPart.setFileName(MimeUtility.encodeText(fileName, charset, encoding));
                multipart.addBodyPart(messageBodyPart);
            }

            messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(body, "text/html; charset=" + charset);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);

            logger.info("sending email to: " + parseObjectToJSON(listToEmail));

            if (!listCcEmail.isEmpty()) {
                logger.info("sending email cc: " + parseObjectToJSON(listCcEmail));
            }

            SMTPTransport transport = (SMTPTransport) session.getTransport("smtp");

            try {
                transport.connect();
                transport.sendMessage(message, message.getAllRecipients());

                String[] lastResponse = transport.getLastServerResponse().split(":");
                if (lastResponse.length > 1) {
                    logger.info("message id: [" + lastResponse[1].substring(1, lastResponse[1].length() - 1) + "]");
                }
            } finally {
                transport.close();
            }

            logger.info("send email success");
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            emailDTO.setError(e.getMessage());
            return emailDTO;
        }

        emailDTO.setStatus(ResponseStatus.SUCCESS);
        return emailDTO;
    }

    public static GetRequestDTO sendSMS(RestTemplate restTemplate, String host, String phoneNumber, String msg) {
        String url = host + "&msisdn=" + phoneNumber + "&msg=" + msg;
        return sendGetRequest(restTemplate, url);
    }

    public static PostRequestDTO sendSMS(RestTemplate restTemplate, String host, String username, String password, String senderName, String phoneNumber, String msg) {
        SMSDTO sms = new SMSDTO();
        sms.setUsername(username);
        sms.setPassword(password);
        sms.setSenderName(senderName);
        sms.setMsisdn(phoneNumber);
        sms.setMessage(msg);
        return sendPostRequest(restTemplate, host, parseObjectToJSON(sms));
    }

    public static PostRequestDTO sendPostRequest(RestTemplate restTemplate, Map<String, String> mapHeader, String url) {
        return sendPostRequest(restTemplate, mapHeader, url, null, contentType);
    }

    public static PostRequestDTO sendPostRequest(RestTemplate restTemplate, Map<String, String> mapHeader, String url, String msg) {
        return sendPostRequest(restTemplate, mapHeader, url, msg, contentType);
    }

    public static PostRequestDTO sendPostRequest(RestTemplate restTemplate, String url) {
        return sendPostRequest(restTemplate, new HashMap<>(), url, null, contentType);
    }

    public static PostRequestDTO sendPostRequest(RestTemplate restTemplate, String url, String msg) {
        return sendPostRequest(restTemplate, new HashMap<>(), url, msg, contentType);
    }

    public static PostRequestDTO sendPostRequest(RestTemplate restTemplate, Map<String, String> mapHeader, String url, String msg, String contentType) {
        logger.info("url:" + url);
        logger.info("message:" + msg);
        logger.info("content-type:" + contentType);
        PostRequestDTO postRequestDTO = new PostRequestDTO();
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", contentType);
        for (String key : mapHeader.keySet()) {
            if (mapHeader.get(key) == null) {
                continue;
            }

            String value = mapHeader.get(key);
            logger.info(key.toLowerCase() + ":" + value);
            headers.add(key, value);
        }

        HttpEntity<Object> entity = new HttpEntity<>(msg, headers);

        ResponseEntity<String> response;
        try {
            response = restTemplate.postForEntity(url, entity, String.class);
        } catch (Exception e) {
            logger.error(e);
            postRequestDTO.setSuccess(false);
            postRequestDTO.setMessage(e.getMessage());
            return postRequestDTO;
        }

        logger.info("response code:[" + response.getStatusCode() + "]");
        logger.info("response body:[" + response.getBody() + "]");

        postRequestDTO.setResponse(response.getBody());
        return postRequestDTO;
    }

    public static GetRequestDTO sendGetRequest(RestTemplate restTemplate, String url) {
        logger.info("url:" + url);
        GetRequestDTO getRequestDTO = new GetRequestDTO();
        ResponseEntity<String> response;
        try {
            response = restTemplate.getForEntity(url, String.class);
        } catch (Exception e) {
            logger.error(e);
            getRequestDTO.setSuccess(false);
            getRequestDTO.setMessage(e.getMessage());
            return getRequestDTO;
        }

        logger.info("response code:[" + response.getStatusCode() + "]");
        logger.info("response body:[" + response.getBody() + "]");

        getRequestDTO.setResponse(response.getBody());
        return getRequestDTO;
    }

    public static String setFileName(FileType type, String prefix, boolean hasDate, boolean hasSuffix, boolean isUpperCaseExtensions) {
        String extensions = type.getFileExtension();

        if (isUpperCaseExtensions) {
            extensions = extensions.toUpperCase();
        }

        String fileName = "";
        fileName += prefix;
        if (hasDate) {
            fileName += "_" + setStringFormatFromDate(new Date(), fileNameDateTimeFormat);
        }
        if (hasSuffix) {
            RandomStringGenerator.Builder random = new RandomStringGenerator.Builder();
            fileName += "_" + random.withinRange('0', 'z').filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS).build().generate(10);
        }
        fileName += extensions;
        return fileName;
    }

    public static String setPathFile(String path, String fileName) {
        if (isNullOrEmpty(path)) {
            path = "";
        }

        path = setTrim(path);
        if (!isNullOrEmpty(path) && !path.endsWith("/")) {
            path += "/";
        }

        return path + fileName;
    }

    public static File setBase64ToFile(String fileData, String fileName) {
        return setBase64ToFile(fileData, "", fileName);
    }

    public static File setBase64ToFile(String fileData, String path, String fileName) {
        createPath(path);

        logger.info("create file " + fileName);
        File file = new File(setPathFile(path, fileName));
        logger.info("change string base64 to byte");
        byte[] data = Base64.getDecoder().decode(fileData);
        logger.info("set zip file min inflate ratio 0");
        ZipSecureFile.setMinInflateRatio(0);
        logger.info("write byte data to file");

        try {
            FileUtils.writeByteArrayToFile(file, data);
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
        logger.info("create file " + fileName + " success");
        return file;
    }

    public static String setFileToBase64(File file) {
        logger.info("set file [" + file.getName() + "] to base64");

        String fileBase64;
        try {
            logger.info("change file to byte");
            byte[] fileByte = FileUtils.readFileToByteArray(file);
            logger.info("change byte to string base64");
            fileBase64 = Base64.getEncoder().encodeToString(fileByte);
        } catch (Exception e) {
            logger.error(e);
            return null;
        }

        logger.info("set file [" + file.getName() + "] to base64 success");
        return fileBase64;
    }

    public static void createPath(String path) {
        if (isNullOrEmpty(path)) {
            return;
        }

        String[] splitPath = path.split("/");
        String newPath = "";
        if (path.startsWith("/")) {
            newPath = "/";
        }

        for (String split : splitPath) {
            newPath += split + "/";
            File filePath = new File(newPath);
            if (!filePath.exists()) {
                logger.info("path [" + newPath + "] is not exists. create path " + ((filePath.mkdir()) ? "success" : "fail"));
            }
        }
    }

    public static String getBackupFileName(String backupFileName) {
        return backupFileName + "_" + setStringFormatFromDate(new Date(), fileNameDateTimeFormat);
    }

    public static void backupFile(File source, String backupPath, String backupFileName) {
        createPath(backupPath);
        String backupFile = setPathFile(backupPath, getBackupFileName(backupFileName));
        File destination = new File(backupFile);
        moveFile(source, destination);
    }

    public static void copyFile(File source, File destination) {
        if (!source.exists()) {
            logger.info("source file [" + source.getPath() + "] is not exists");
            return;
        }

        boolean isSuccess = true;
        try {
            FileUtils.copyFile(source, destination);
        } catch (Exception e) {
            logger.error(e);
            isSuccess = false;
        }
        logger.info("copy file [" + source.getPath() + "] to [" + destination.getPath() + "] " + ((isSuccess) ? "success" : "fail"));
    }

    public static void moveFile(File source, File destination) {
        if (!source.exists()) {
            logger.info("source file [" + source.getPath() + "] is not exists");
            return;
        }

        boolean isMoved = source.renameTo(destination);
        logger.info("move file [" + source.getPath() + "] to [" + destination.getPath() + "] " + ((isMoved) ? "success" : "fail"));
    }

    public static void deleteFile(File file) {
        if (file == null) {
            return;
        }

        if (!file.exists()) {
            logger.info("file [" + file.getPath() + "] is not exists cannot delete");
            return;
        }

        logger.info("delete file [" + file.getPath() + "] " + (file.delete() ? "success" : "fail"));
    }

    public static void getTime(Date start, Date end) {
        logger.info(getTextTime(start, end));
    }

    public static String getTextTime(Date start, Date end) {
        long time = end.getTime() - start.getTime();
        long timeInSecond = time / 1000;
        long hour = timeInSecond / 3600;
        long minute = (timeInSecond / 60) - (hour * 60);
        long second = timeInSecond % 60;
        long millisecond = time % 1000;
        if (millisecond > 0) {
            return "time:[" + time + "] hour:[" + hour + "] minute:[" + minute + "] second:[" + second + "] millisecond:[" + millisecond + "]";
        }
        return null;
    }

    public static PageableDTO getPageable(RequestDTO req) {
        return getPageable(req, Sort.unsorted());
    }

    public static PageableDTO getPageable(RequestDTO req, Sort sort) {
        PageableDTO pageableDTO = new PageableDTO();
        if (isNullOrEmpty(req.getPageNumber())) {
            pageableDTO.setStatus(ResponseStatus.MISSING_PAGE_NUMBER);
            return pageableDTO;
        }

        if (isNullOrEmpty(req.getPageSize())) {
            pageableDTO.setStatus(ResponseStatus.MISSING_PAGE_SIZE);
            return pageableDTO;
        }

        if (!isNumeric(req.getPageNumber())) {
            pageableDTO.setStatus(ResponseStatus.INVALID_PAGE_NUMBER);
            return pageableDTO;
        }

        if (!isNumeric(req.getPageSize())) {
            pageableDTO.setStatus(ResponseStatus.INVALID_PAGE_SIZE);
            return pageableDTO;
        }

        int pageNumber = Integer.parseInt(req.getPageNumber());
        int pageSize = Integer.parseInt(req.getPageSize());

        if (pageSize < 1) {
            pageableDTO.setStatus(ResponseStatus.INVALID_PAGE_SIZE_LESS_THAN_MIN);
            return pageableDTO;
        }

        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);
        pageableDTO.setStatus(ResponseStatus.SUCCESS);
        pageableDTO.setPageable(pageable);
        return pageableDTO;
    }

    public static ResponseBodyDTO setResponseBody(ResponseBodyDTO res, ResponseStatus status) {
        return setResponseBody(res, status, languageThai);
    }

    public static ResponseBodyDTO setResponseBody(ResponseBodyDTO res, ResponseStatus status, String language) {
        return setResponseBody(res, status, null, language);
    }

    public static ResponseBodyDTO setResponseBody(ResponseBodyDTO res, ResponseStatus status, String description, String language) {
        String message = status.getDescEn();
        if (isNullOrEmpty(language)) {
            language = "";
        }

        if (language.equals(languageThai)) {
            message = status.getDescTh();
        }

        if (!isNullOrEmpty(description)) {
            message += " (" + description + ")";
        }

        res.setMessage(message);
        res.setCode(status.getCode());
        return res;
    }

    public static String setCamelCaseToSnakeCase(String text) {
        List<String> listText = new ArrayList<>();
        listText.add(text);
        List<String> listSnakeCase = setCamelCaseToSnakeCase(listText);
        return listSnakeCase.get(0);
    }

    public static List<String> setCamelCaseToSnakeCase(List<String> listText) {
        List<String> listNewText = new ArrayList<>();
        for (String text : listText) {
            StringBuilder newText = new StringBuilder();
            for (int i = 0; i < text.length(); i++) {
                char character = text.charAt(i);
                if (i > 0) {
                    char previousCharacter = text.charAt(i - 1);
                    if (previousCharacter != ' ') {
                        if (Character.isUpperCase(character)) {
                            newText.append("_");
                        }
                    }
                }

                if (character != ' ') {
                    newText.append(character);
                }
            }
            listNewText.add(newText.toString().toLowerCase());
        }
        return listNewText;
    }

    public static String setSnakeCaseToCamelCase(String text) {
        List<String> listText = new ArrayList<>();
        listText.add(text);
        List<String> listCamelCase = setSnakeCaseToCamelCase(listText);
        return listCamelCase.get(0);
    }

    public static List<String> setSnakeCaseToCamelCase(List<String> listText) {
        List<String> listNewText = new ArrayList<>();
        char underscore = '_';
        for (String text : listText) {
            StringBuilder newText = new StringBuilder();
            for (int i = 0; i < text.length(); i++) {
                char character = text.charAt(i);
                if (character == ' ' || character == underscore) {
                    continue;
                }

                if (i - 1 > 0) {
                    char previousCharacter = text.charAt(i - 1);
                    if (previousCharacter == underscore) {
                        newText.append(Character.toUpperCase(character));
                        continue;
                    }
                }
                newText.append(character);
            }
            listNewText.add(newText.toString());
        }
        return listNewText;
    }

    public static void getRecord(Integer record) {
        getRecord(record.longValue());
    }

    public static void getRecord(long record) {
        logger.info(getTextRecord(record));
    }

    public static String getTextRecord(long record) {
        if (record == 0) {
            return "not found any record";
        }

        return "found " + setFinancialAmount(BigInteger.valueOf(record), 0, 0, true) + " record" + ((record == 1) ? "" : "s");
    }

    public static long getRound(Integer count, Integer maximum) {
        return getRound(count.longValue(), maximum.longValue());
    }

    public static long getRound(Long count, Integer maximum) {
        return getRound(count, maximum.longValue());
    }

    public static long getRound(Long count, Long maximum) {
        long size = count / maximum;
        if (Math.floorMod(count, maximum) != 0 || count == 0) {
            size++;
        }
        return size;
    }

    public static int getStart(int num, int maximum) {
        return num * maximum;
    }

    public static int getStop(int num, int maximum, Long maximumData) {
        return Math.min(((num + 1) * maximum), maximumData.intValue());
    }

    public static int getStop(int num, int maximum, int maximumData) {
        return Math.min(((num + 1) * maximum), maximumData);
    }

    public static void getPercent(double num, double maximum, double condition) {
        getPercent(BigDecimal.valueOf(num), BigDecimal.valueOf(maximum), BigDecimal.valueOf(condition));
    }

    public static void getPercent(long num, long maximum, long condition) {
        getPercent(BigDecimal.valueOf(num), BigDecimal.valueOf(maximum), BigDecimal.valueOf(condition));
    }

    public static void getPercent(int num, int maximum, int condition) {
        getPercent(BigDecimal.valueOf(num), BigDecimal.valueOf(maximum), BigDecimal.valueOf(condition));
    }

    public static void getPercent(BigDecimal num, BigDecimal maximum, BigDecimal condition) {
        BigDecimal percent = num.multiply(BigDecimal.valueOf(100)).divide(maximum);
        if (percent.remainder(condition).equals(BigDecimal.ZERO)) {
            logger.info("percent: " + percent + "% (" + num + "/" + maximum + ")");
        }

        if (num.add(BigDecimal.ONE).equals(maximum)) {
            logger.info("percent: 100% (" + num.add(BigDecimal.ONE) + "/" + maximum + ")");
        }
    }

    public static String setData(String text) {
        if (text == null) {
            text = "";
        }

        return setData(text, text.length(), new TextDTO());
    }

    public static String setData(String text, TextDTO textDTO) {
        if (text == null) {
            text = "";
        }

        return setData(text, text.length(), textDTO);
    }

    public static String setData(String text, int length) {
        if (text == null) {
            text = "";
        }

        return setData(text, length, new TextDTO());
    }

    public static String setData(String text, String delimiter) {
        if (text == null) {
            text = "";
        }

        TextDTO textDTO = new TextDTO();
        textDTO.setDelimiter(delimiter);
        return setData(text, text.length(), textDTO);
    }

    public static String setData(String text, int length, TextDTO textDTO) {
        if (isNullOrEmpty(text)) {
            text = "";
        }

        text = text.trim();

        String format = "%";

        if (textDTO.isPadding()) {
            if (textDTO.isBack()) {
                format += "-";
            }
            format += length;
        }

        format += "s";

        if (length != 0) {
            text = String.format(format, text.substring(0, Math.min(length, text.length())));
        }

        if (text.length() > length) {
            text = text.substring(0, length);
        }
        return text + textDTO.getDelimiter();
    }

    public static String getText(String[] text, Sequence sequence) {
        if (sequence.getSequence() == null) {
            sequence.setSequence(0);
            return text[0];
        }

        sequence.setSequence(sequence.getSequence() + 1);
        return text[sequence.getSequence()];
    }
}
