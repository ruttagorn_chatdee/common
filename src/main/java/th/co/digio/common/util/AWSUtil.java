package th.co.digio.common.util;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static th.co.digio.common.util.CommonUtil.isNullOrEmpty;
import static th.co.digio.common.util.CommonUtil.setPathFile;

public class AWSUtil {

    private static final Logger logger = LogManager.getLogger(CommonUtil.class);

    public static AmazonS3 getAmazonS3(String accessKey, String secretKey) {
        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        return AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.AP_SOUTHEAST_1).build();
    }

    public static boolean uploadFile(String bucketName, String accessKey, String secretKey, String path, File file) {
        return uploadFile(bucketName, accessKey, secretKey, path, file, false);
    }

    public static boolean uploadFile(String bucketName, String accessKey, String secretKey, String path, File file, boolean isPublic) {
        List<File> listFile = new ArrayList<>();
        listFile.add(file);
        return uploadFile(bucketName, accessKey, secretKey, path, listFile, isPublic);
    }

    public static boolean uploadFile(String bucketName, String accessKey, String secretKey, String path, List<File> listFile) {
        return uploadFile(bucketName, accessKey, secretKey, path, listFile, false);
    }

    public static boolean uploadFile(String bucketName, String accessKey, String secretKey, String path, List<File> listFile, boolean isPublic) {
        logger.info("upload file");
        logger.info("bucket name:" + bucketName);
        try {
            AmazonS3 s3client = getAmazonS3(accessKey, secretKey);
            for (File file : listFile) {
                String fileName = file.getName();
                String destination = setPathFile(path, fileName);

                logger.info("uploading");
                logger.info("source:" + file.getPath());
                logger.info("destination:" + destination);

                if (isPublic) {
                    s3client.putObject(new PutObjectRequest(bucketName, destination, file).withCannedAcl(CannedAccessControlList.PublicRead));
                } else {
                    s3client.putObject(bucketName, destination, file);
                }
                logger.info("upload file [" + file.getPath() + "] to [" + destination + "] success");
            }
        } catch (Exception e) {
            logger.info(e);
            return false;
        }
        return true;
    }

    public static boolean downloadFile(String bucketName, String accessKey, String secretKey, String source, String destination) {
        logger.info("download file");
        logger.info("bucket name:" + bucketName);
        logger.info("source:" + source);
        logger.info("destination:" + destination);

        try {
            AmazonS3 s3client = getAmazonS3(accessKey, secretKey);
            S3Object object = s3client.getObject(bucketName, source);
            FileUtils.copyInputStreamToFile(object.getObjectContent(), new File(destination));
        } catch (Exception e) {
            logger.info(e);
            return false;
        }

        logger.info("download file [" + source + "] to [" + destination + "] success");
        return true;
    }

    public static String getUrlPath(String bucketName, String accessKey, String secretKey, String path, String fileName) {
        logger.info("get url path");
        String url = "";
        String key = "";
        try {
            AmazonS3 s3client = getAmazonS3(accessKey, secretKey);

            List<S3ObjectSummary> list = s3client.listObjects(bucketName, setPathFile(path, fileName)).getObjectSummaries();

            for (S3ObjectSummary l : list) {
                key = l.getKey();
            }

            if (!isNullOrEmpty(key)) {
                GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, key);
                url = s3client.generatePresignedUrl(generatePresignedUrlRequest).toString();
            }
        } catch (Exception e) {
            logger.error(e);
            return url;
        }

        logger.info("url path:" + url);
        logger.info("get url path success");
        return url;
    }

    public static boolean replaceFile(String accessKey, String secretKey, String sourceBucketName, String sourcePath, String sourceFileName, String destinationBucketName, String destinationPath, String destinationFileName) {
        logger.info("copy file");
        logger.info("copy from bucket name:[" + sourceBucketName + "] path:[" + sourcePath + "] file name:[" + sourceFileName + "] to bucket name:[" + destinationBucketName + "] path:[" + destinationPath + "] file name:[" + destinationFileName + "]");
        try {
            AmazonS3 s3client = getAmazonS3(accessKey, secretKey);
            s3client.copyObject(sourceBucketName, setPathFile(sourcePath, sourceFileName), destinationBucketName, setPathFile(destinationPath, destinationFileName));
            logger.info("replace file success");
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            return false;
        }
        return true;
    }

    public static boolean deleteFile(String bucketName, String accessKey, String secretKey, String path, String fileName) {
        List<String> listFileName = new ArrayList<>();
        listFileName.add(fileName);
        return deleteFile(bucketName, accessKey, secretKey, path, listFileName);
    }

    public static boolean deleteFile(String bucketName, String accessKey, String secretKey, String path, List<String> listFileName) {
        logger.info("delete file");
        logger.info("bucket name:[" + bucketName + "]");
        try {
            AmazonS3 s3client = getAmazonS3(accessKey, secretKey);

            for (String fileName : listFileName) {
                String deleteFile = setPathFile(path, fileName);
                logger.info("delete file:[" + deleteFile + "]");
                s3client.deleteObject(bucketName, deleteFile);
                logger.info("delete file:[" + deleteFile + "] success");
            }
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            return false;
        }
        return true;
    }

    public static List<String> listAllFileInPath(String bucketName, String accessKey, String secretKey, String path) {
        logger.info("find all file in path");
        logger.info("bucket name:[" + bucketName + "]");

        AmazonS3 s3client = getAmazonS3(accessKey, secretKey);

        List<S3ObjectSummary> listObject = s3client.listObjects(bucketName, path).getObjectSummaries();
        List<String> listFileName = new ArrayList<>();
        for (S3ObjectSummary list : listObject) {
            listFileName.add(list.getKey());
        }

        return listFileName;
    }
}
