package th.co.digio.common.util;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import th.co.digio.common.enumuration.FileType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZIPFile extends CommonUtil {

    private static final Logger logger = LogManager.getLogger(ZIPFile.class);

    public static File ZipFile(String path, String fileName, List<File> listFile) {
        return ZipFile(path, fileName, listFile, null);
    }

    public static File ZipFile(String path, String fileName, List<File> listFile, String password) {
        return ZipFile(path, fileName, listFile, password, false, false, false, EncryptionMethod.ZIP_STANDARD);
    }

    public static File ZipFile(String path, String fileName, List<File> listFile, String password, boolean hasDate, boolean hasSuffix, boolean isUpperExtensions, EncryptionMethod encryptionMethod) {
        createPath(path);

        File file = new File(setPathFile(path, setFileName(FileType.ZIP, fileName, hasDate, hasSuffix, isUpperExtensions)));
        ZipFile zipFile;
        try {
            if (isNullOrEmpty(password)) {
                zipFile = new ZipFile(file);
                zipFile.addFiles(listFile);
            } else {
                ZipParameters zipParameters = new ZipParameters();
                zipParameters.setEncryptFiles(true);
                zipParameters.setEncryptionMethod(encryptionMethod);
                zipFile = new ZipFile(file, password.toCharArray());
                zipFile.addFiles(listFile, zipParameters);
            }
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
        return zipFile.getFile();
    }

    private static File UnzipFile(String path, String fileZip) {
        File file = new File(path);
        try {
            byte[] buffer = new byte[1024];
            InputStream input = new FileInputStream(fileZip);
            ZipInputStream inputFile = new ZipInputStream(input);
            ZipEntry zipEntry = inputFile.getNextEntry();
            while (zipEntry != null) {
                File newFile = new File(file, zipEntry.getName());

                String destDirPath = file.getCanonicalPath();
                String destFilePath = newFile.getCanonicalPath();

                if (!destFilePath.startsWith(destDirPath + File.separator)) {
                    continue;
                }

                if (zipEntry.isDirectory()) {
                    if (!newFile.isDirectory() && !newFile.mkdirs()) {
                        continue;
                    }
                } else {
                    File parent = newFile.getParentFile();
                    if (!parent.isDirectory() && !parent.mkdirs()) {
                        continue;
                    }

                    FileOutputStream output = new FileOutputStream(newFile);
                    int len;
                    while ((len = inputFile.read(buffer)) > 0) {
                        output.write(buffer, 0, len);
                    }
                    output.close();
                }
                zipEntry = inputFile.getNextEntry();
            }
            inputFile.closeEntry();
            inputFile.close();
        } catch (Exception e) {
            logger.error(e);
        }
        return file;
    }

}

