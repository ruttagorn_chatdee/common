package th.co.digio.common.util;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import th.co.digio.common.dto.csv.CSVData;
import th.co.digio.common.dto.csv.CSVSuffixDTO;
import th.co.digio.common.enumuration.FileType;

import java.io.*;
import java.util.List;

public class CSVFile extends CommonUtil {

    private static final Logger logger = LogManager.getLogger(CSVFile.class);

    public static File createFile(String path, String fileName, List<CSVData> listBody) {
        CSVSuffixDTO suffixDTO = new CSVSuffixDTO();
        return createFile(path, fileName, listBody, suffixDTO);
    }

    public static File createFile(String path, String fileName, List<CSVData> listBody, String charset) {
        CSVSuffixDTO suffixDTO = new CSVSuffixDTO();
        suffixDTO.setCharset(charset);
        return createFile(path, fileName, listBody, suffixDTO);
    }

    public static File createFile(String path, String fileName, List<CSVData> listBody, CSVSuffixDTO suffixDTO) {
        createPath(path);

        File file = new File(setPathFile(path, setFileName(FileType.CSV, fileName, suffixDTO.isHasDate(), suffixDTO.isHasRandomSuffix(), suffixDTO.isUpperExtensions())));
        try {
            OutputStream outputStream = new FileOutputStream(file);
            Writer outputFile = new OutputStreamWriter(outputStream, suffixDTO.getCharset());

            CSVWriter writer = new CSVWriter(outputFile);

            for (CSVData body : listBody) {
                writer.writeNext(body.getData().toArray(new String[0]), body.isHasDoubleQuote());
            }

            writer.close();
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
        return file;
    }

    public static List<String[]> readFile(String fileData) {
        return readFile("", fileData);
    }

    public static List<String[]> readFile(String path, String fileData) {
        File file = setBase64ToFile(fileData, path, setFileName(FileType.CSV, "file", true, true, false));
        return readFile(file, true);
    }

    public static List<String[]> readFile(File file) {
        return readFile(file, false);
    }

    public static List<String[]> readFile(File file, boolean deleteFile) {
        if (file == null) {
            return null;
        }

        List<String[]> listData;
        try {
            FileReader fileReader = new FileReader(file);
            CSVReader reader = new CSVReader(fileReader);
            listData = reader.readAll();

            if (deleteFile) {
                deleteFile(file);
            }
        } catch (Exception e) {
            logger.error(e);
            return null;
        }

        return listData;
    }
}

