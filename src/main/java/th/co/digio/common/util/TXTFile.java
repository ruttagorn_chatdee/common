package th.co.digio.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import th.co.digio.common.enumuration.FileType;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class TXTFile extends CommonUtil {

    private static final Logger logger = LogManager.getLogger(TXTFile.class);

    public static File createFile(String path, String fileName, List<String> listText) {
        return createFile(path, fileName, listText, false, true, StandardCharsets.UTF_8);
    }

    public static File createFile(String path, String fileName, List<String> listText, String charset) {
        return createFile(path, fileName, listText, false, false, true, charset);
    }

    public static File createFile(String path, String fileName, List<String> listText, boolean hasSuffix) {
        return createFile(path, fileName, listText, hasSuffix, true, StandardCharsets.UTF_8);
    }

    public static File createFile(String path, String fileName, List<String> listText, boolean hasSuffix, String charset) {
        return createFile(path, fileName, listText, false, hasSuffix, true, charset);
    }

    public static File createFile(String path, String fileName, List<String> listText, boolean hasSuffix, boolean isUpperCaseExtensions, Charset charset) {
        return createFile(path, fileName, listText, false, hasSuffix, isUpperCaseExtensions, charset.name());
    }

    public static File createFile(String path, String fileName, List<String> listText, boolean hasDate, boolean hasSuffix, boolean isUpperCaseExtensions, String charset) {
        createPath(path);

        File file = new File(setPathFile(path, setFileName(FileType.TXT, fileName, hasDate, hasSuffix, isUpperCaseExtensions)));
        try {
            FileOutputStream output = new FileOutputStream(file);
            OutputStreamWriter outputWriter = new OutputStreamWriter(output, charset);
            BufferedWriter bufferedWriter = new BufferedWriter(outputWriter);
            for (String text : listText) {
                bufferedWriter.write(text);
                bufferedWriter.newLine();
            }

            bufferedWriter.close();
        } catch (Exception e) {
            logger.error(e);
            return null;
        }

        return file;
    }
}

