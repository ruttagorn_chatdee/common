package th.co.digio.common.util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import th.co.digio.common.dto.sftp.SFTPFileDTO;
import th.co.digio.common.enumuration.ResponseStatus;

import java.io.File;
import java.util.*;

public class SFTPUtil extends CommonUtil {
    public static final Logger logger = LogManager.getLogger(SFTPUtil.class);

    private final String channelType = "sftp";

    public Session connect(String host, int port, String username, String password, String key) {
        logger.info("connecting");
        logger.info("host:" + host);
        logger.info("port:" + port);
        logger.info("username:" + username);

        JSch jsch = new JSch();
        Session session;
        try {
            if (!isNullOrEmpty(key)) {
                jsch.addIdentity(key);
            }
            session = jsch.getSession(username, host, port);
            session.setPassword(password);
            session.setConfig("StrictHostKeyChecking", "no");
            session.connect();
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
        logger.info("connected");
        return session;
    }

    public boolean upload(Session session, String source, String destination) {
        logger.info("upload file");
        logger.info("source:" + source);
        logger.info("destination:" + destination);

        try {
            Channel channel = session.openChannel(channelType);
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;

            logger.info("uploading");

            sftpChannel.put(source, destination);
            sftpChannel.exit();
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
        logger.info("upload file success");
        return true;
    }

    public SFTPFileDTO download(String host, int port, String username, String password, String key, String source, String destination) {
        return download(host, port, username, password, key, source, destination, new ArrayList<>());
    }

    public SFTPFileDTO download(String host, int port, String username, String password, String key, String source, String destination, List<String> listFileName) {
        SFTPFileDTO sftpFileDTO = new SFTPFileDTO();
        Session session = connect(host, port, username, password, key);
        if (session == null) {
            return sftpFileDTO;
        }

        sftpFileDTO = download(session, source, destination, listFileName);

        if (!disconnect(session)) {
            sftpFileDTO.setStatus(ResponseStatus.ERROR_EXCEPTION_OCCURRED);
            return sftpFileDTO;
        }

        return sftpFileDTO;
    }

    public SFTPFileDTO download(Session session, String source, String destination) {
        return download(session, source, destination, new ArrayList<>());
    }

    public SFTPFileDTO download(Session session, String source, String destination, String fileName) {
        return download(session, source, destination, new ArrayList<>(List.of(fileName)));
    }

    public SFTPFileDTO download(Session session, String source, String destination, List<String> listFileName) {
        SFTPFileDTO sftpFileDTO = new SFTPFileDTO();
        logger.info("download file");
        logger.info("source:" + source);
        logger.info("destination:" + destination);
        if (!listFileName.isEmpty()) {
            for (String filename : listFileName) {
                logger.info("filename:" + filename);
            }
        }

        try {
            Channel channel = session.openChannel(channelType);
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            downloadFile(sftpChannel, sftpFileDTO, source, destination, listFileName);
            sftpChannel.exit();
            channel.disconnect();
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
            return sftpFileDTO;
        }
        return sftpFileDTO;
    }

    public boolean delete(Session session, String exportFileDestination, String fileName) {
        List<String> listFileName = new ArrayList<>();
        listFileName.add(fileName);
        return delete(session, exportFileDestination, listFileName);
    }

    public boolean delete(Session session, String exportFileDestination, List<String> listFileName) {
        logger.info("delete file");
        for (String fileName : listFileName) {
            logger.info("file :" + fileName);
        }

        try {
            Channel channel = session.openChannel(channelType);
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            Vector<ChannelSftp.LsEntry> list = sftpChannel.ls(exportFileDestination);

            for (ChannelSftp.LsEntry item : list) {
                String fileName = item.getFilename();
                String destination = setPathFile(exportFileDestination, fileName);
                if (listFileName.contains(fileName)) {
                    logger.info("delete file:[" + destination + "]");
                    sftpChannel.rm(destination);
                    logger.info("delete file:[" + destination + "] success");
                }
            }
            sftpChannel.exit();
        } catch (Exception e) {
            logger.error(e);
            return false;
        }

        return true;
    }

    public boolean disconnect(Session session) {
        logger.info("disconnecting");

        try {
            if (session == null) {
                return false;
            }

            if (session.isConnected()) {
                session.disconnect();
            }
        } catch (Exception e) {
            logger.error(e);
            return false;
        }
        logger.info("disconnected");
        return true;
    }

    private void downloadFile(ChannelSftp sftpChannel, SFTPFileDTO sftpFileDTO, String sourcePath, String destPath, List<String> listSpecificFileName) {
        downloadFile(sftpChannel, sftpFileDTO, sourcePath, destPath, listSpecificFileName, false);
    }

    private void downloadFile(ChannelSftp sftpChannel, SFTPFileDTO sftpFileDTO, String sourcePath, String destPath, List<String> listSpecificFileName, boolean overwrite) {

        createPath(destPath);

        File listFile = new File(destPath);
        String[] file = listFile.list();
        if (file == null) {
            String error = "path is not exists";
            logger.error(error);
            sftpFileDTO.setStatus(ResponseStatus.ERROR_EXCEPTION_OCCURRED);
            sftpFileDTO.setDescription(error);
            return;
        }

        List<String> listFileName = Arrays.asList(file);
        Vector<ChannelSftp.LsEntry> list;
        try {
            list = sftpChannel.ls(sourcePath);
        } catch (Exception e) {
            logger.error(e);
            sftpFileDTO.setStatus(ResponseStatus.ERROR_EXCEPTION_OCCURRED);
            sftpFileDTO.setDescription(ExceptionUtils.getStackTrace(e));
            return;
        }

        list.removeIf(lsEntry -> {
            String fileName = lsEntry.getFilename();
            if (overwrite) {
                return fileName.equals(".") || fileName.equals("..");
            } else {
                return listFileName.contains(fileName) || fileName.equals(".") || fileName.equals("..");
            }
        });

        List<String> listFileDownloaded = new ArrayList<>();

        for (ChannelSftp.LsEntry item : list) {
            String fileName = item.getFilename();
            if (item.getAttrs().isDir()) {
                downloadFile(sftpChannel, sftpFileDTO, setPathFile(sourcePath, fileName), setPathFile(destPath, fileName), listSpecificFileName);
            } else {
                //if specific name is null or empty download all file in directory
                //else download only specific name
                if (listSpecificFileName.isEmpty()) {
                    download(sftpChannel, sftpFileDTO, sourcePath, destPath, fileName);
                } else if (listSpecificFileName.contains(fileName)) {
                    download(sftpChannel, sftpFileDTO, sourcePath, destPath, fileName);
                    listFileDownloaded.add(fileName);
                }
            }
        }

        if (listSpecificFileName.isEmpty() && !sftpFileDTO.isFileFound()) {
            logger.info("folder [" + sourcePath + "] is not found any file");
        } else {
            listSpecificFileName.removeAll(listFileDownloaded);
            for (String specificFileName : listSpecificFileName) {
                logger.info("file [" + specificFileName + "] is not found");
                sftpFileDTO.setStatus(ResponseStatus.NOT_FOUND_FILE);
            }
        }
    }

    public void download(ChannelSftp sftpChannel, SFTPFileDTO sftpFileDTO, String sourcePath, String destPath, String fileName) {
        try {
            logger.info("downloading file [" + fileName + "]");
            sftpChannel.get(setPathFile(sourcePath, fileName), setPathFile(destPath, fileName));
            logger.info("downloaded file [" + fileName + "]");
        } catch (Exception e) {
            logger.error(e);
            sftpFileDTO.setStatus(ResponseStatus.NOT_FOUND_FILE);
            return;
        }
        sftpFileDTO.setFileFound(true);
        sftpFileDTO.setStatus(ResponseStatus.SUCCESS);
    }

    public List<String> listFileName(Session session, String sourcePath) {
        List<String> listFileName = new ArrayList<>();

        try {
            Channel channel = session.openChannel(channelType);
            channel.connect();
            ChannelSftp sftpChannel = (ChannelSftp) channel;
            Vector<ChannelSftp.LsEntry> list = sftpChannel.ls(sourcePath);
            for (ChannelSftp.LsEntry item : list) {
                String fileName = item.getFilename();
                if (fileName.equals(".") || fileName.equals("..")) {
                    continue;
                }
                if (item.getAttrs().isDir()) {
                    listFileName.addAll(listFileName(session, setPathFile(sourcePath, fileName)));
                } else {
                    listFileName.add(setPathFile(sourcePath, fileName));
                }
            }
        } catch (Exception e) {
            logger.error(e);
        }

        listFileName.sort(Comparator.naturalOrder());
        return listFileName;
    }
}
