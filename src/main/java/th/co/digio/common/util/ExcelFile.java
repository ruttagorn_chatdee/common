package th.co.digio.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.crypt.EncryptionMode;
import org.apache.poi.poifs.crypt.Encryptor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import th.co.digio.common.dto.excel.ExcelDTO;
import th.co.digio.common.dto.excel.ExcelData;
import th.co.digio.common.dto.excel.ExcelSheet;
import th.co.digio.common.enumuration.FileType;

import java.awt.font.FontRenderContext;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.text.AttributedString;
import java.util.*;

public class ExcelFile extends CommonUtil {

    private static final FontRenderContext fontRenderContext = new FontRenderContext(null, true, true);

    public static final Logger logger = LogManager.getLogger(ExcelFile.class);

    public static short percent = 10;

    public static File createFile(String path, String fileName, List<ExcelSheet> listSheet) {
        return createFile(path, fileName, listSheet, new ArrayList<>(), null, false, false, false);
    }

    public static File createFile(String path, String fileName, List<ExcelSheet> listSheet, String password) {
        return createFile(path, fileName, listSheet, new ArrayList<>(), password, false, false, false);
    }

    public static File createFile(String path, String fileName, List<ExcelSheet> listSheet, List<String> listMergeCell) {
        return createFile(path, fileName, listSheet, listMergeCell, null, false, false, false);
    }

    public static File createFile(String path, String fileName, List<ExcelSheet> listSheet, List<String> listMergeCell, String password) {
        return createFile(path, fileName, listSheet, listMergeCell, password, false, false, false);
    }

    public static File createFile(String path, String fileName, List<ExcelSheet> listSheet, List<String> listMergeCell, String password, boolean hasDate, boolean hasSuffix, boolean isUpperCaseExtensions) {
        logger.info("create file:[" + setPathFile(path, fileName) + "]");
        createPath(path);

        XSSFWorkbook wb = new XSSFWorkbook();
        SXSSFWorkbook workbook = new SXSSFWorkbook(wb);
        workbook.setCompressTempFiles(true);

        double defaultCharWidth = getDefaultCharWidth(workbook);
        Map<String, CellStyle> mapCellStyle = new HashMap<>();

        for (ExcelSheet sheetDTO : listSheet) {
            SXSSFSheet sheet;
            try {
                sheet = workbook.createSheet(sheetDTO.getSheetName());
            } catch (Exception e) {
                logger.error(e);
                return null;
            }

            for (String mergeCell : listMergeCell) {
                sheet.addMergedRegion(CellRangeAddress.valueOf(mergeCell));
            }

            List<ExcelData> listData = sheetDTO.getRow();

            Map<Integer, Double> mapCellWidth = new HashMap<>();

            logger.info("start set data to file sheet:[" + sheetDTO.getSheetName() + "]");
            logger.info("total row: " + setFinancialAmount(BigInteger.valueOf(listData.size()), true));

            Date start = new Date();

            int column = 0;
            int rowIndex = 0;
            for (ExcelData listColumn : listData) {
                SXSSFRow row = sheet.createRow(rowIndex++);
                int cellNum = 0;
                for (ExcelDTO data : listColumn.getData()) {
                    SXSSFCell cell = row.createCell(cellNum++);
                    cell.setCellStyle(getExcelStyle(mapCellStyle, workbook, data));

                    CellType cellType = data.getCellType();
                    if (cellType == CellType.FORMULA) {
                        cell.setCellFormula(data.getText());
                    } else if (cellType == CellType.NUMERIC) {
                        if (!isNullOrEmpty(data.getText())) {
                            double num;
                            try {
                                num = Double.parseDouble(data.getText());
                                cell.setCellValue(num);
                            } catch (Exception ignored) {

                            }
                        }
                    } else {
                        cell.setCellType(cellType);
                        cell.setCellValue(data.getText());
                    }

                    double width = getColumnWidth(wb, row, cell, sheet.getMergedRegions(), defaultCharWidth);
                    Double cellWidth = mapCellWidth.get(cellNum - 1);

                    if (cellWidth == null) {
                        cellWidth = 0D;
                    }

                    mapCellWidth.put(cellNum - 1, Math.max(cellWidth, width));
                }

                column = Math.max(cellNum, column);
            }

            Date end = new Date();
            logger.info("finish set data to file sheet:[" + sheetDTO.getSheetName() + "]");
            getTime(start, end);

            logger.info("start auto size column sheet:[" + sheetDTO.getSheetName() + "]");
            start = new Date();
            for (int j = 0; j < column; j++) {
                Double width = mapCellWidth.get(j);
                if (width == null) {
                    width = 0D;
                }

                autoSizeColumn(sheet, j, width);
            }
            end = new Date();
            logger.info("finish auto size column sheet:[" + sheetDTO.getSheetName() + "]");
            getTime(start, end);
        }

        logger.info("creating file:[" + setPathFile(path, fileName) + "]");
        Date start = new Date();
        File file = new File(setPathFile(path, setFileName(FileType.EXCEL, fileName, hasDate, hasSuffix, isUpperCaseExtensions)));
        try {
            FileOutputStream output = new FileOutputStream(file);
            if (!isNullOrEmpty(password)) {

                POIFSFileSystem fs = new POIFSFileSystem();
                EncryptionInfo info = new EncryptionInfo(EncryptionMode.agile);
                Encryptor enc = info.getEncryptor();
                enc.confirmPassword(password);
                OutputStream os = enc.getDataStream(fs);
                workbook.write(os);
                workbook.close();
                os.close();
                fs.writeFilesystem(output);
                output.close();
                fs.close();

            } else {
                workbook.write(output);
                workbook.close();
                output.flush();
                output.close();
            }

        } catch (Exception e) {
            logger.error(e);
            return null;
        }

        logger.info("created file:[" + file.getPath() + "]");
        Date end = new Date();
        getTime(start, end);
        return file;
    }

    public static List<ExcelSheet> readFile(File file) {
        List<ExcelSheet> listExcelSheet = new ArrayList<>();

        SXSSFWorkbook workbook = getWorkbook(file);
        if (workbook == null) {
            return null;
        }

        int numberOfSheets = workbook.getXSSFWorkbook().getNumberOfSheets();

        for (int num = 0; num < numberOfSheets; num++) {

            List<ExcelData> listExcelData = new ArrayList<>();

            logger.info("set data from file to list");
            XSSFSheet sheet = getSheet(workbook, num);
            int totalRow = sheet.getLastRowNum() + 2;
            for (int rowNum = 0; rowNum < totalRow; rowNum++) {
                List<ExcelDTO> data = new ArrayList<>();
                boolean hasData = false;

                XSSFRow row = sheet.getRow(rowNum);
                if (row == null) {
                    continue;
                }

                int totalCell = row.getLastCellNum();
                for (int cellNum = 0; cellNum < totalCell; cellNum++) {
                    ExcelDTO excelDTO = new ExcelDTO();
                    XSSFCell cell = row.getCell(cellNum);
                    if (cell == null) {
                        excelDTO.setText("");
                        data.add(excelDTO);
                        continue;
                    }

                    DataFormatter dataFormatter = new DataFormatter();
                    String dataCell = dataFormatter.formatCellValue(cell);

                    if (isNullOrEmpty(dataCell)) {
                        dataCell = "";
                    }

                    dataCell = dataCell.trim();
                    if (!isNullOrEmpty(dataCell)) {
                        hasData = true;
                    }

                    excelDTO.setText(dataCell);
                    data.add(excelDTO);
                }

                if (hasData) {
                    ExcelData excelData = new ExcelData();
                    excelData.setData(data);
                    listExcelData.add(excelData);
                }
            }

            ExcelSheet excelSheet = new ExcelSheet();
            excelSheet.setRow(listExcelData);
            excelSheet.setSheetName(sheet.getSheetName());
            listExcelSheet.add(excelSheet);
        }

        return listExcelSheet;
    }

    public static List<ExcelSheet> readFile(String fileData) {
        return readFile("", fileData);
    }

    public static List<ExcelSheet> readFile(String path, String fileData) {
        File file = setBase64ToFile(fileData, path, setFileName(FileType.EXCEL, "file", true, true, false));
        if (file == null) {
            return null;
        }

        return readFile(file);
    }

    public static File updateFile(String pathSource, String fileData, String pathDestination, String fileName, List<List<ExcelDTO>> listColumn, List<List<ExcelDTO>> listRow) {
        SXSSFWorkbook workbook = getWorkbook(pathSource, fileData);
        if (workbook == null) {
            return null;
        }
        return updateFile(pathDestination, fileName, workbook, listColumn, listRow);
    }

    public static File updateFile(String path, String fileName, File file, List<List<ExcelDTO>> listColumn, List<List<ExcelDTO>> listRow) {
        SXSSFWorkbook workbook = getWorkbook(file);
        if (workbook == null) {
            return null;
        }
        return updateFile(path, fileName, workbook, listColumn, listRow);
    }

    public static File updateFile(String path, String fileName, SXSSFWorkbook workbook, List<List<ExcelDTO>> listColumn, List<List<ExcelDTO>> listRow) {
        XSSFSheet sheet = getSheet(workbook, 0);

        Map<String, CellStyle> mapCellStyle = new HashMap<>();
        logger.info("set data from file to list");
        int lastCell = 0;
        for (Row row : sheet) {
            for (List<ExcelDTO> listExcelDTO : listColumn) {
                if (row.getRowNum() >= listExcelDTO.size()) {
                    break;
                }

                if (row.getLastCellNum() > lastCell) {
                    lastCell = row.getLastCellNum();
                }

                Cell cell = row.createCell(lastCell);
                ExcelDTO excelDTO = listExcelDTO.get(row.getRowNum());
                cell.setCellStyle(getExcelStyle(mapCellStyle, workbook, excelDTO));
                cell.getCellStyle().setVerticalAlignment(VerticalAlignment.TOP);
                cell.setCellType(excelDTO.getCellType());
                cell.setCellValue(excelDTO.getText());
            }
        }

        int rowIndex = sheet.getLastRowNum();
        for (List<ExcelDTO> listExcelDTO : listRow) {
            XSSFRow row = sheet.createRow(rowIndex++);
            int cellNum = 0;
            for (ExcelDTO data : listExcelDTO) {
                XSSFCell cell = row.createCell(cellNum++);

                cell.setCellStyle(getExcelStyle(mapCellStyle, workbook, data));
                cell.setCellType(data.getCellType());
                cell.setCellValue(data.getText());
            }
        }

        for (int j = 0; j < lastCell + 1; j++) {
            sheet.autoSizeColumn(j);
        }

        File file = new File(setPathFile(path, setFileName(FileType.EXCEL, fileName, false, false, false)));
        try {
            FileOutputStream output = new FileOutputStream(file);
            workbook.write(output);
            workbook.close();
            output.flush();
            output.close();
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
            return null;
        }
        return file;
    }


    private static SXSSFWorkbook getWorkbook(String path, String fileData) {
        File file = setBase64ToFile(fileData, path, setFileName(FileType.EXCEL, "file", true, true, false));
        if (file == null) {
            return null;
        }

        SXSSFWorkbook workbook = getWorkbook(file);
        deleteFile(file);
        return workbook;
    }

    private static SXSSFWorkbook getWorkbook(File file) {
        logger.info("set xssf workbook");
        XSSFWorkbook wb;
        try {
            wb = new XSSFWorkbook(file);
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
            return null;
        }

        logger.info("set sxssf workbook");
        return new SXSSFWorkbook(wb);
    }

    private static XSSFSheet getSheet(SXSSFWorkbook workbook, int numberOfSheet) {
        logger.info("set compress temp file in workbook");
        workbook.setCompressTempFiles(true);
        return workbook.getXSSFWorkbook().getSheetAt(numberOfSheet);
    }

    private static void setBorder(CellStyle cellStyle, boolean isBorder) {
        BorderStyle borderStyle = (isBorder) ? BorderStyle.THIN : BorderStyle.NONE;
        cellStyle.setBorderBottom(borderStyle);
        cellStyle.setBorderTop(borderStyle);
        cellStyle.setBorderLeft(borderStyle);
        cellStyle.setBorderRight(borderStyle);
    }

    private static void setFont(CellStyle cellStyle, SXSSFWorkbook workbook, boolean bold) {
        Font font = workbook.createFont();
        font.setBold(bold);
        font.setFontName("Arial");
        cellStyle.setFont(font);
    }

    private static CellStyle getExcelStyle(Map<String, CellStyle> mapCellStyle, SXSSFWorkbook workbook, ExcelDTO data) {
        String key = data.getAlignment() + "_" + data.isBorder() + "_" + data.isBold() + "_" + data.isPercent() + "_" + data.getDataFormat();
        CellStyle cellStyle = mapCellStyle.get(key);
        if (cellStyle == null) {
            cellStyle = workbook.createCellStyle();
            setBorder(cellStyle, data.isBorder());
            setFont(cellStyle, workbook, data.isBold());
            cellStyle.setAlignment(data.getAlignment());
            cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

            short dataFormat = 0;
            if (data.isPercent()) {
                dataFormat = percent;
            } else if (!isNullOrEmpty(data.getDataFormat())) {
                dataFormat = workbook.createDataFormat().getFormat(data.getDataFormat());
            }

            cellStyle.setDataFormat(dataFormat);
            mapCellStyle.put(key, cellStyle);
        }
        return cellStyle;
    }

    public static void setExcelData(List<ExcelDTO> list) {
        setExcelData(list, null, HorizontalAlignment.CENTER, CellType.STRING, new ExcelDTO());
    }

    public static void setExcelDataLeft(List<ExcelDTO> list, String text) {
        setExcelData(list, text, HorizontalAlignment.LEFT, CellType.STRING, new ExcelDTO());
    }

    public static void setExcelDataLeft(List<ExcelDTO> list, String text, boolean border) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBorder(border);
        setExcelData(list, text, HorizontalAlignment.LEFT, CellType.STRING, excelDTO);
    }

    public static void setExcelDataRight(List<ExcelDTO> list, String text) {
        setExcelData(list, text, HorizontalAlignment.RIGHT, CellType.STRING, new ExcelDTO());
    }

    public static void setExcelData(List<ExcelDTO> list, String text) {
        setExcelData(list, text, HorizontalAlignment.CENTER, CellType.STRING, new ExcelDTO());
    }

    public static void setExcelData(List<ExcelDTO> list, String text, ExcelDTO excelDTO) {
        setExcelData(list, text, excelDTO.getAlignment(), excelDTO.getCellType(), excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, String text, HorizontalAlignment alignment) {
        setExcelData(list, text, alignment, CellType.STRING, new ExcelDTO());
    }

    public static void setExcelData(List<ExcelDTO> list, String text, boolean border) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBorder(border);
        setExcelData(list, text, HorizontalAlignment.CENTER, CellType.STRING, excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, boolean bold, String text) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBold(bold);
        setExcelData(list, text, HorizontalAlignment.CENTER, CellType.STRING, excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, boolean bold, String text, HorizontalAlignment alignment) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBold(bold);
        setExcelData(list, text, alignment, CellType.STRING, excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, String text, HorizontalAlignment alignment, boolean border) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBorder(border);
        setExcelData(list, text, alignment, CellType.STRING, excelDTO);
    }

    public static void setExcelDataLeft(List<ExcelDTO> list, String text, CellType cellType) {
        setExcelData(list, text, HorizontalAlignment.LEFT, cellType, new ExcelDTO());
    }

    public static void setExcelDataLeft(List<ExcelDTO> list, String text, boolean border, CellType cellType) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBorder(border);
        setExcelData(list, text, HorizontalAlignment.LEFT, cellType, excelDTO);
    }

    public static void setExcelDataRight(List<ExcelDTO> list, String text, CellType cellType) {
        setExcelData(list, text, HorizontalAlignment.RIGHT, cellType, new ExcelDTO());
    }

    public static void setExcelData(List<ExcelDTO> list, String text, CellType cellType) {
        setExcelData(list, text, HorizontalAlignment.CENTER, cellType, new ExcelDTO());
    }

    public static void setExcelData(List<ExcelDTO> list, String text, HorizontalAlignment alignment, CellType cellType) {
        setExcelData(list, text, alignment, cellType, new ExcelDTO());
    }

    public static void setExcelData(List<ExcelDTO> list, String text, boolean border, CellType cellType) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBorder(border);
        setExcelData(list, text, HorizontalAlignment.CENTER, cellType, excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, boolean bold, String text, CellType cellType) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBold(bold);
        setExcelData(list, text, HorizontalAlignment.CENTER, cellType, excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, boolean bold, String text, HorizontalAlignment alignment, CellType cellType) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBold(bold);
        setExcelData(list, text, alignment, cellType, excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, String text, HorizontalAlignment alignment, boolean border, CellType cellType) {
        ExcelDTO excelDTO = new ExcelDTO();
        excelDTO.setBorder(border);
        setExcelData(list, text, alignment, cellType, excelDTO);
    }

    public static void setExcelData(List<ExcelDTO> list, String text, HorizontalAlignment alignment, CellType cellType, ExcelDTO excelDTO) {
        ExcelDTO data = new ExcelDTO();
        data.setText(text);
        data.setAlignment(alignment);
        data.setBorder(excelDTO.isBorder());
        data.setBold(excelDTO.isBold());
        data.setCellType(cellType);
        data.setPercent(excelDTO.isPercent());
        data.setDataFormat(excelDTO.getDataFormat());
        list.add(data);
    }

    public static void addExcelRow(List<ExcelData> listRow, List<ExcelDTO> listData) {
        ExcelData data = new ExcelData();
        data.setData(listData);
        listRow.add(data);
    }

    public static void addExcelSheet(List<ExcelSheet> listSheet, List<ExcelData> listRow, String sheetName) {
        ExcelSheet sheet = new ExcelSheet();
        sheet.setRow(listRow);
        sheet.setSheetName(sheetName);
        listSheet.add(sheet);
    }

    public static void autoSizeColumn(SXSSFSheet sheet, int column, double cellWidth) {
        int activeWidth = (int) (256.0D * cellWidth);
        int bestFitWidth = Math.max(-256, activeWidth);
        if (bestFitWidth > 0) {
            int width = Math.min(bestFitWidth, 65280);
            sheet.setColumnWidth(column, width);
        }
    }

    public static double getColumnWidth(Workbook wb, SXSSFRow row, SXSSFCell cell, List<CellRangeAddress> listMergeRegions, double defaultCharWidth) {
        int column = cell.getColumnIndex();
        double colspan = 1;

        for (CellRangeAddress region : listMergeRegions) {
            if (region.isInRange(row.getRowNum(), column)) {
                cell = row.getCell(region.getFirstColumn());
                colspan = 1 + region.getLastColumn() - region.getFirstColumn();
                break;
            }
        }

        CellStyle style = cell.getCellStyle();
        CellType cellType = cell.getCellType();
        if (cellType == CellType.FORMULA) {
            cellType = cell.getCachedFormulaResultType();
        }

        Font font = wb.getFontAt(style.getFontIndexAsInt());

        double width = -1.0D;
        if (cellType == CellType.STRING) {
            String text = cell.getStringCellValue();
            String[] lines = text.split("\\n");

            for (String line : lines) {
                String txt = line + '0';
                AttributedString str = new AttributedString(txt);
                copyAttributes(font, str, txt.length());
                width = getCellWidth(width, colspan, defaultCharWidth, style.getIndention(), str);
            }
        } else {
            String text = null;
            if (cellType == CellType.NUMERIC) {
                try {
                    DataFormatter formatter = new DataFormatter();
                    text = formatter.formatCellValue(cell);
                } catch (Exception var23) {
                    text = String.valueOf(cell.getNumericCellValue());
                }
            } else if (cellType == CellType.BOOLEAN) {
                text = String.valueOf(cell.getBooleanCellValue()).toUpperCase(Locale.ROOT);
            }

            if (text != null) {
                String txt = text + '0';
                AttributedString str = new AttributedString(txt);
                copyAttributes(font, str, txt.length());
                width = getCellWidth(width, colspan, defaultCharWidth, style.getIndention(), str);
            }
        }

        return Math.max(-1.0D, width);
    }

    private static double getCellWidth(double minWidth, double colspan, double defaultCharWidth, double indention, AttributedString str) {
        TextLayout layout = new TextLayout(str.getIterator(), fontRenderContext);
        Rectangle2D bounds = layout.getBounds();
        return Math.max(minWidth, (bounds.getX() + bounds.getWidth()) / colspan / defaultCharWidth + indention);
    }

    private static void copyAttributes(Font font, AttributedString str, int endIdx) {
        str.addAttribute(TextAttribute.FAMILY, font.getFontName(), 0, endIdx);
        str.addAttribute(TextAttribute.SIZE, (float) font.getFontHeightInPoints());
        if (font.getBold()) {
            str.addAttribute(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD, 0, endIdx);
        }

        if (font.getItalic()) {
            str.addAttribute(TextAttribute.POSTURE, TextAttribute.POSTURE_OBLIQUE, 0, endIdx);
        }

        if (font.getUnderline() == 1) {
            str.addAttribute(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON, 0, endIdx);
        }
    }

    public static int getDefaultCharWidth(Workbook wb) {
        Font defaultFont = wb.getFontAt(0);
        AttributedString str = new AttributedString(String.valueOf('0'));
        copyAttributes(defaultFont, str, 1);
        TextLayout layout = new TextLayout(str.getIterator(), fontRenderContext);
        return (int) layout.getAdvance();
    }
}