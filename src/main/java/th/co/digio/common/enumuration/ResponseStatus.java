package th.co.digio.common.enumuration;

public enum ResponseStatus {

    SUCCESS("0000", "success", "ทำรายการสำเร็จ"),

    MISSING_PAGE_NUMBER("1000", "please input page number", "กรุณาระบุเลขที่ของหน้า"),
    MISSING_PAGE_SIZE("1001", "please input page size", "กรุณาระบุจำนวนขนาดต่อหน้า"),

    INVALID_PAGE_NUMBER("2000", "invalid page number", "เลขที่ของหน้าไม่ถูกต้อง"),
    INVALID_PAGE_SIZE("2001", "invalid page size", "จำนวนขนาดต่อหน้าไม่ถูกต้อง"),
    INVALID_PAGE_SIZE_LESS_THAN_MIN("2002", "page size must not be less than one", "จำนวนขนาดต่อหน้าต้องไม่น้อยกว่า 1"),

    NOT_FOUND_FILE("3001", "Not found file", "ไม่พบไฟล์"),

    ERROR_EXCEPTION_OCCURRED("9999", "system error occurred", "ระบบเกิดเหตุขัดข้อง กรุณาลองทำรายการใหม่ภายหลัง");

    private final String code;
    private final String descEn;
    private final String descTh;

    ResponseStatus(final String code, final String descEn, final String descTh) {
        this.code = code;
        this.descEn = descEn;
        this.descTh = descTh;
    }

    public String getDescTh() {
        return descTh;
    }

    public String getCode() {
        return code;
    }

    public String getDescEn() {
        return descEn;
    }

    public static ResponseStatus getStatusCode(final String value) {
        for (ResponseStatus response : values()) {
            if (response.code.equalsIgnoreCase(value)) {
                return response;
            }
        }
        return null;
    }
}
