package th.co.digio.common.enumuration;

public enum CommonStatus {
    ACTIVE("ACTIVE"),
    PENDING("PENDING"),
    SUSPENDED("SUSPENDED"),
    TERMINATED("TERMINATED"),
    ;

    private final String value;

    CommonStatus(final String value) {
        this.value = value;
    }

    public String getStatus() {
        return value;
    }

    public static CommonStatus getStatus(final String value) {
        for (CommonStatus status : values()) {
            if (status.value.equalsIgnoreCase(value)) {
                return status;
            }
        }
        return null;
    }
}
