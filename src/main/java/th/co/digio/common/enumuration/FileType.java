package th.co.digio.common.enumuration;

public enum FileType {
    PDF("PDF", ".pdf"),
    EXCEL("EXCEL", ".xlsx"),
    CSV("CSV", ".csv"),
    TXT("TXT", ".txt"),
    ZIP("ZIP", ".zip"),
    ;

    private final String type;
    private final String extension;

    FileType(String type, String extension) {
        this.type = type;
        this.extension = extension;
    }

    public String getFileType() {
        return type;
    }

    public String getFileExtension() {
        return extension;
    }

    public static FileType getFileByType(String type) {
        for (FileType fileType : values()) {
            if (fileType.type.equals(type)) {
                return fileType;
            }
        }
        return null;
    }

    public static FileType getFileByExtension(String extension) {
        for (FileType fileType : values()) {
            if (fileType.extension.equals(extension)) {
                return fileType;
            }
        }
        return null;
    }
}
